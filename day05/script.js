$(document).ready(function () {
    $(function () {
      $("#btnRegister").on("click", function () {
        $("#form-register").validate({
          rules: {
            hovaten: {
              required: true,
            },
            gioitinh: {
              required: true,
            },
            khoa: {
              required: true,
            },
            ngaysinh: {
              required: true,
              dateITA: true,
            },
          },
          messages: {
            hovaten: {
              required: "Hãy nhập tên."
            },
            gioitinh: {
              required: "Hãy chọn giới tính."
            },
            khoa: {
              required: "Hãy chọn phân khoa."
            },
            ngaysinh: {
              required: "Hãy nhập ngày sinh.",
              dateITA: "Hãy nhập ngày sinh đúng định dạng.",
            },
          },
          errorPlacement: function (error, element) {
            var placement = $(element).data("error");
            console.log(placement);
            if (placement) {
              $(placement).append(error);
            } else {
              error.insertAfter(element);
            }
          },
        });
      });
    });
  });
  