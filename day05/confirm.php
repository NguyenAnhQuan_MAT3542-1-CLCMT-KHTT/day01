<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trang đăng ký</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="./style.css">
</head>

<body>
    <?php
    $gioitinh = [
        0 => "Nam",
        1 => "Nữ"
    ];

    $khoa = [
        "MAT" => "Khoa học máy tính",
        "KDL" => "Khoa học dữ liệu"
    ];
    ?>
    <div class="container form-register-container">
        <form action="" method="POST" id="form-register">
            <table class="confirm-table">
                <tr>
                    <td>
                        <label class="information" for="hovaten">Họ và tên</label>
                    </td>
                    <td>
                        <p><?php echo $_POST["hovaten"]; ?></p>
                    </td>
                </tr>
                <tr>
                    <td><label class="information" for="gioitinh">Giới tính</label></td>
                    <td>
                        <p><?php echo $_POST["gioitinh"]; ?></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="information" for="phankhoa">Phân khoa</label>
                    </td>
                    <td>
                        <p><?php echo $_POST["khoa"]; ?></p>
                    </td>
                </tr>
                <tr>
                    <td><label class="information" for="ngaysinh">Ngày sinh</label></td>
                    <td>
                        <p><?php echo $_POST["ngaysinh"]; ?></p>
                    </td>
                </tr>
                <tr>
                    <td><label class="information label-diachi" for="diachi">Địa chỉ</label></td>
                    <td>
                        <p><?php echo $_POST["diachi"]; ?></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="information  label-image" for="img">Hình ảnh</label>
                    </td>
                    <td>
                        <?php
                        if (isset($_POST["btnRegister"])) {
                            $target_dir = "uploads/";
                            $target_file = $target_dir . basename($_FILES["image"]["name"]);
    

                            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                                echo "<img class='img' src='" . $target_file . "' alt='Uploaded Image' width = '100px' height='auto' >";
                            } else {
                                echo "Sorry, there was an error uploading your file.";
                            }
                        }
                        ?>

                    </td>
                </tr>
            </table>
            <div class="center-button">
                <button id="btnRegister" class="register-btn" type="submit">Xác nhận</button>
            </div>
        </form>
    </div>
    <script src="./script.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/additional-methods.js"></script>
</body>

</html>