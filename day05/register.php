<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trang đăng ký</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="./style.css">
</head>

<body>
    <?php
    $gioitinh = [
        0 => "Nam",
        1 => "Nữ"
    ];

    $khoa = [
        "MAT" => "Khoa học máy tính",
        "KDL" => "Khoa học dữ liệu"
    ];
    ?>
    <div class="container form-register-container">
        <form action="./confirm.php" method="POST" id="form-register" enctype="multipart/form-data">
            <div class="errorTxt">
                <p id="errNm1"></p>
                <p id="errNm2"></p>
                <p id="errNm3"></p>
                <p id="errNm4"></p>

            </div>
            <table>
                <tr>
                    <td>
                        <label class="information" for="hovaten">Họ và tên <span style="color: red">*</span></label>
                    </td>
                    <td>
                        <div><input class="hovaten" name="hovaten" type="text" data-error="#errNm1"></div>
                    </td>
                </tr>
                <tr>
                    <td><label class="information" for="gioitinh">Giới tính <span style="color: red">*</span></label></td>
                    <td>
                        <?php
                        for ($i = 0; $i < count($gioitinh); $i++) {
                            echo "<input class='gioitinh' type='radio' name='gioitinh' value='$gioitinh[$i]' data-error='#errNm2' > $gioitinh[$i]";
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="information" for="phankhoa">Phân khoa <span style="color: red">*</span></label>
                    </td>
                    <td>
                        <select name="khoa" id="khoa" class="khoa" data-error="#errNm3">
                            <option value="">--Chọn phân khoa--</option>
                            <?php foreach ($khoa as $value) { ?>
                                <option value="<?php echo "$value"; ?>"><?php echo "$value" ?></option>

                            <?php } ?>
                            ?>
                        </select>

                    </td>
                </tr>
                <tr>
                    <td><label class="information" for="ngaysinh">Ngày sinh <span style="color: red">*</span></label></td>
                    <td>
                        <input class="ngaysinh" name="ngaysinh" type="text" placeholder="dd/mm/yyyy" data-error="#errNm4">
                    </td>
                </tr>
                <tr>
                    <td><label class="information label-diachi" for="diachi">Địa chỉ</label></td>
                    <td>
                        <input class="diachi" name="diachi" type="text">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="information" for="img">Hình ảnh</label>
                    </td>
                    <td>
                        <input class="image" type="file" name="image" id="image">
                    </td>
                </tr>
            </table>
            <div class="center-button">
                <button id="btnRegister" name="btnRegister" class="register-btn" type="submit">Đăng ký</button>
            </div>
        </form>
    </div>
    <script src="./script.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/additional-methods.js"></script>
</body>

</html>