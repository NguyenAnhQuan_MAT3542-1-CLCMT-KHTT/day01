<?php
include("database.php");

// Truy vấn để lấy toàn bộ dữ liệu ban đầu
$query = $conn->prepare("SELECT ID, HoVaTen, PhanKhoa FROM students");
$query->execute();
$result = $query->get_result();
$students = $result->fetch_all(MYSQLI_ASSOC);

// Trả về kết quả dưới dạng JSON
echo json_encode($students);
?>
