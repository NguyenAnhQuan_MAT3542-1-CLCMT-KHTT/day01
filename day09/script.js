$(document).ready(function () {
  $(function () {
    $("#btnRegister").on("click", function () {
      $("#form-register").validate({
        rules: {
          hovaten: {
            required: true,
          },
          gioitinh: {
            required: true,
          },
          khoa: {
            required: true,
          },
          ngaysinh: {
            required: true,
            dateITA: true,
          },
        },
        messages: {
          hovaten: {
            required: "Hãy nhập tên.",
          },
          gioitinh: {
            required: "Hãy chọn giới tính.",
          },
          khoa: {
            required: "Hãy chọn phân khoa.",
          },
          ngaysinh: {
            required: "Hãy nhập ngày sinh.",
            dateITA: "Hãy nhập ngày sinh đúng định dạng.",
          },
        },
        errorPlacement: function (error, element) {
          var placement = $(element).data("error");
          console.log(placement);
          if (placement) {
            $(placement).append(error);
          } else {
            error.insertAfter(element);
          }
        },
      });
    });
  });
  $(document).on('click',  '.btn-Del', function() {
    var row = $(this).closest("tr");
    var id = row.find("td:first").text();
    $('.btn-Del-Confirm').on('click', function() {
      deleteStudent(id);
    });
  });

  $(document).on('click', '.btn-Mod', function () {
    // Assuming you have a unique identifier (e.g., student ID) in the first column
    var id = $(this).closest("tr").find("td:first").text();

    // Redirect to update_students.php with the selected ID
    window.location.href = 'update_students.php?id=' + id;
});


});

function resetForm() {
  document.getElementById("khoa").value = "";
  document.getElementById("tu-khoa").value = "";
  $.ajax({
    type: "POST",
    url: "resetTable.php", // Đường dẫn đến file xử lý AJAX (cần tạo)
    success: function (data) {
      // Hiển thị lại toàn bộ dữ liệu ban đầu
      displayStudents(data);
    },
    error: function (xhr, status, error) {
      // Xử lý lỗi nếu có
      console.error(xhr.responseText);
    },
  });
}

$(document).ready(function () {
  $("#khoa").change(function () {
    searchStudents();
  });
  $("#tu-khoa").keyup(function () {
    // Gọi hàm xử lý AJAX khi có sự kiện "keyup"
    searchStudents();
  });
});

function searchStudents() {
  var khoa = $("#khoa").val();
  var keyword = $("#tu-khoa").val();

  $.ajax({
    type: "POST",
    url: "searchStudents.php",
    data: { khoa: khoa, keyword: keyword },
    success: function (data) {
      displayStudents(data);
    },
    error: function (xhr, status, error) {
      console.error(xhr.responseText);
    },
  });
}

function displayStudents(data) {
  var students = JSON.parse(data);
  var numStudent = students.length;
  var table = $("#studentTable");
  document.getElementById("count-student").innerHTML =
    "Số sinh viên tìm thấy: " + numStudent;
  table.find("tr:gt(0)").remove();

  $.each(students, function (index, student) {
    var row =
      "<tr>" +
      "<td>" +
      student.ID +
      "</td>" +
      "<td>" +
      student.HoVaTen +
      "</td>" +
      "<td>" +
      student.PhanKhoa +
      "</td>" +
      "<td>" +
      '<button class="btn-Del" type="button" data-toggle="modal" data-target="#exampleModalCenter"">Xóa</button>' +
      '<button class="btn-Mod">Sửa</button>' +
      "</td>" +
      "</tr>";
    table.append(row);
  });
}

function deleteStudent(studentID) {
  
  $.ajax({
    type: "POST",
    url: "delete_students.php",
    data: { ID: studentID },
    success: function () {
      searchStudents();
    },
    error: function (xhr, status, error) {
      console.error(xhr.responseText);
    },
  });
}
