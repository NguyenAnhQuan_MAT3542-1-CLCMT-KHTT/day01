<html>

<head>
    <title>List Students</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="./style2.css">
    <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <script src="script.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>

<body>
    <?php
    include("database.php");

    $khoa = [
        "MAT" => "Khoa học máy tính",
        "KDL" => "Khoa học dữ liệu"
    ];


    $query = $conn->prepare("SELECT ID, HoVaTen, GioiTinh, NgaySinh, DiaChi, HinhAnh, PhanKhoa FROM students");
    $query->execute();
    $students = $query->get_result();
    $students = $students->fetch_all(MYSQLI_ASSOC);
    ?>

    <!-- Button trigger modal -->
    <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
        Launch demo modal
    </button> -->

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Xóa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Bạn muốn xóa sinh viên này?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-Del-Confirm" data-dismiss="modal">Xóa</button>
                    <button type="button" class="btn btn-primary btn-Cancle" data-dismiss="modal">Hủy</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container form-container">
        <form method="post">

            <div class="tim-kiem-khoa">
                <label class="ten-khoa" for="ten-khoa">Khoa</label>
                <select name="khoa" id="khoa" class="khoa">
                    <option value=""></option>
                    <?php foreach ($khoa as $value) { ?>
                        <option value="<?php echo "$value"; ?>"><?php echo "$value" ?></option>

                    <?php } ?>
                    ?>
                </select>
            </div>
            <div class="tim-kiem-tu-khoa">
                <label>Từ khóa</label>
                <input type="text" name="tu-khoa" id="tu-khoa" class="tu-khoa">
            </div>
            <button type="button" onclick="resetForm()" class='btn-Search'>Reset</button>
        </form>
    </div>

    <div class="container">
        <p class='count-student' id='count-student'> Số sinh viên tìm thấy: <?php echo count($students) ?></p>
        <button class="btn-Add" onclick="window.location.href='register.php';">Thêm</button>
    </div>
    <table id="studentTable">
        <tr>
            <td>No</td>
            <td>Tên sinh viên</td>
            <td>Khoa</td>
            <td>Action</td>
        </tr>
        <?php
        foreach ($students as $key => $value) {
        ?>
            <tr>
                <td>
                    <?php echo $value['ID'];
                    ?>
                </td>
                <td>
                    <?php echo $value['HoVaTen'];
                    ?>
                </td>
                <td>
                    <?php echo $value['PhanKhoa'];
                    ?>
                </td>
                <td>
                    <input type='hidden' name='id' value="$value['ID']">
                    <button class="btn-Del" type="button" data-toggle="modal" data-target="#exampleModalCenter">Xóa</button>
                    <button class="btn-Mod">Sửa</button>
                </td>
            </tr>
        <?php } ?>
    </table>