CREATE DATABASE IF NOT EXISTS ltweb;
USE ltweb;
CREATE TABLE IF NOT EXISTS students(
    ID INT AUTO_INCREMENT,
    HoVaTen varchar(30) NOT NULL,
    GioiTinh varchar(3) NOT NULL,
    PhanKhoa varchar(30) NOT NULL,
    NgaySinh date NOT NULL,
    DiaChi varchar(50),
    HinhAnh varchar(50),
    PRIMARY KEY(ID)
);
