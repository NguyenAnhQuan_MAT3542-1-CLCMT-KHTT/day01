<?php
include("database.php");

// Lấy khoa từ AJAX
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['ID'])) {
    $id = $_POST['ID'];

    $query = $conn->prepare("DELETE FROM students WHERE ID = ?");
    $query->bind_param("s",$id);
    $query->execute();
}
?>
