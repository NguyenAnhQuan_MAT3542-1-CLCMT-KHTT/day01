<?php
include("database.php");

// Lấy từ khóa từ AJAX
$keyword = $_POST['keyword'];

// Xử lý tìm kiếm trong CSDL theo từ khóa
$query = $conn->prepare("SELECT ID, HoVaTen, PhanKhoa FROM students WHERE ID LIKE ? OR HoVaTen LIKE ? OR PhanKhoa LIKE ? ");
$keyword = "%$keyword%";
$query->bind_param("sss", $keyword, $keyword, $keyword);
$query->execute();
$result = $query->get_result();
$students = $result->fetch_all(MYSQLI_ASSOC);

// Trả về kết quả dưới dạng JSON
echo json_encode($students);
?>