<?php
ob_start();
include "database.php";

// Check if the 'id' parameter is set in the URL
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    // Fetch student information based on the provided ID
    $query = $conn->prepare("SELECT ID, HoVaTen, GioiTinh, NgaySinh, DiaChi, HinhAnh, PhanKhoa FROM students WHERE ID = ?");
    $query->bind_param("s", $id);
    $query->execute();
    $result = $query->get_result();
    $students = $result->fetch_assoc();
    if ($students) {
?>
        <!DOCTYPE html>
        <html lang="vi">

        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Trang update</title>
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" />
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"></script>
            <link rel="stylesheet" href="./style.css">
        </head>

        <body>
            <?php
            $gioitinh = [
                0 => "Nam",
                1 => "Nữ"
            ];

            $khoa = [
                "MAT" => "Khoa học máy tính",
                "KDL" => "Khoa học dữ liệu"
            ];
            ?>
            <div class="container form-register-container">
                <form action="" method="POST" id="form-register" enctype="multipart/form-data">
                    <div class="errorTxt">
                        <p id="errNm1"></p>
                        <p id="errNm2"></p>
                        <p id="errNm3"></p>
                        <p id="errNm4"></p>

                    </div>
                    <table>
                        <tr>
                            <input type="hidden" name="id" id="id" value="<?php echo $students['ID'] ?>">
                            <td>
                                <label class="information" for="hovaten">Họ và tên <span style="color: red">*</span></label>
                            </td>
                            <td>
                                <input class="hovaten" name="hovaten" id="hovaten" type="text" value="<?php echo $students['HoVaTen']; ?>" data-error="#errNm1">
                            </td>
                        </tr>
                        <tr>
                            <td><label class="information" for="gioitinh">Giới tính <span style="color: red">*</span></label></td>
                            <td>
                                <?php
                                for ($i = 0; $i < count($gioitinh); $i++) {
                                    $isChecked = ($gioitinh[$i] == $students['GioiTinh']) ? 'checked' : '';
                                    echo "<input class='gioitinh' type='radio' name='gioitinh' value='$gioitinh[$i]' $isChecked data-error='#errNm2'> $gioitinh[$i]";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="information" for="phankhoa">Phân khoa <span style="color: red">*</span></label>
                            </td>
                            <td>
                                <select name="khoa" id="khoa" class="khoa" data-error="#errNm3">
                                    <option value="<?php echo $students['PhanKhoa']; ?>"><?php echo $students['PhanKhoa']; ?></option>
                                    <?php foreach ($khoa as $value) { ?>
                                        <option value="<?php echo "$value"; ?>"><?php echo "$value" ?></option>

                                    <?php } ?>
                                    ?>
                                </select>

                            </td>
                        </tr>
                        <input type="hidden" name="date" id="date" value="<?php echo $students['NgaySinh'] ?>">
                        <tr>
                            <td><label class="information" for="ngaysinh">Ngày sinh <span style="color: red">*</span></label></td>
                            <td>
                                <input class="ngaysinh" name="ngaysinh" type="text" value="<?php echo date("d/m/Y", strtotime($students['NgaySinh'])); ?>" placeholder="dd/mm/yyyy"  data-error="#errNm4">
                            </td>
                        </tr>
                        <tr>
                            <td><label class="information label-diachi" for="diachi">Địa chỉ</label></td>
                            <td>
                                <input class="diachi" name="diachi" type="text" value=" <?php echo $students['DiaChi']; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="information" for="img">Hình ảnh</label>
                            </td>
                            <td>
                                <input class="image" type="file" name="image" id="image">
                            </td>
                        </tr>
                    </table>
                    <div class="center-button">
                        <button id="btnRegister" name="btnConfirm" class="register-btn" type="submit">Cập nhật</button>
                    </div>
                </form>

                <?php
                include 'database.php';

                if (isset($_POST['btnConfirm'])) {
                    $id = $_POST['id'];
                    $ho_va_ten = $_POST['hovaten'];
                    $ngay_sinh = $_POST['ngaysinh'];
                    $gioi_tinh = $_POST['gioitinh'];
                    $khoa = $_POST['khoa'];
                    $dia_chi = $_POST['diachi'];
                    $query = $conn->prepare("UPDATE students SET HoVaTen = ?, NgaySinh = STR_TO_DATE(?,'%d/%m/%Y'), GioiTinh = ?, PhanKhoa = ? , DiaChi = ? WHERE ID = ?");
                    $query->bind_param("ssssss", $ho_va_ten, $ngay_sinh, $gioi_tinh, $khoa, $dia_chi, $id);
                    $query->execute();
                    if ($query->execute() === TRUE) {
                        echo "Dữ liệu đã được lưu vào cơ sở dữ liệu thành công!";
                        header("Location: display.php");
                        

                    } else {
                        echo "Lỗi: " . $sql . "<br>" . $conn->error;
                    }

                    $conn->close();
                }
                ?>



            </div>
            <script src="./script.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/additional-methods.js"></script>
        </body>

        </html>
<?php
    } else {
        echo "Student not found.";
    }
} else {
    echo "Invalid request. Please provide a student ID.";
}
ob_end_flush();
?>