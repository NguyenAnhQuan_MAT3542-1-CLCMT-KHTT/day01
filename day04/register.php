<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trang đăng ký</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"></script>
    <style>
        .form-register-container {
            width: 500px;
            height: 510px;
            border: 2px solid #527ea5;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            position: fixed;
            border-radius: 5px;
        }

        .information {
            width: 120px;
            height: 35px;
            margin: 5px;
            padding: 10px;
            background-color: #70ad47;
            color: #fff;
            box-sizing: border-box;
            display: block;
            text-align: center;
            border: 2px solid #527ea5;
            font-family: 'Century';
        }

        /* input[type="radio"] {
            background-color: #5b9bd5;
        } */

        .gioitinh {
            margin-left: 20px;
        }

        .hovaten {
            width: 275px;
            height: 35px;
            border: 1px solid #000;
            margin: 10px;
            padding: 10px;
            box-sizing: border-box;
            display: flex;
            align-items: center;
            border: 2px solid #527ea5;
            font-family: 'Century';
        }

        .khoa {
            width: 145px;
            height: 35px;
            margin: 10px;
            border: 2px solid #527ea5;
        }

        .ngaysinh {
            width: 145px;
            height: 35px;
            margin: 10px;
            border: 2px solid #527ea5;
            padding-left: 10px;
        }

        .diachi {
            width: 275px;
            height: 80px;
            margin: 10px;
            border: 2px solid #527ea5;
        }

        .center-button {
            display: flex;
            justify-content: center;
            text-align: center;
        }

        .register-btn {
            background-color: #70ad47;
            width: 125px;
            height: 40px;
            margin-top: 20px;
            border: 2px solid #527ea5;
            border-radius: 8px;
            color: white;
            font-family: Century;
            font-size: 14px;
        }

        table {
            margin: 100px 20px 0 25px;
        }

        select {
            appearance: none;
            text-align-last: center;
        }

        textarea {
            resize: none;
        }

        .errorTxt {
            line-height: 1px;
            margin-left: 30px;
            margin-top: 10px;
            top: 10px;
            left: 30px;
            color: red;
            display: block;
            position: absolute;
            font-family: 'Century';
        }

        .label-diachi {
            margin-bottom: 55px;
        }
    </style>
</head>

<body>
    <?php
    $gioitinh = [
        0 => "Nam",
        1 => "Nữ"
    ];

    $khoa = [
        "MAT" => "Khoa học máy tính",
        "KDL" => "Khoa học dữ liệu"
    ];
    ?>
    <div class="container form-register-container">
        <form action="" method="POST" id="form-register">
            <div class="errorTxt">
                <p id="errNm1"></p>
                <p id="errNm2"></p>
                <p id="errNm3"></p>
                <p id="errNm4"></p>

            </div>
            <table>
                <tr>
                    <td>
                        <label class="information" for="hovaten">Họ và tên <span style="color: red">*</span></label>
                    </td>
                    <td>
                        <div><input class="hovaten" name="hovaten" type="text" data-error="#errNm1"></div>
                    </td>
                </tr>
                <tr>
                    <td><label class="information" for="gioitinh">Giới tính <span style="color: red">*</span></label></td>
                    <td>
                        <?php
                        for ($i = 0; $i < count($gioitinh); $i++) {
                            echo "<input class='gioitinh' type='radio' name='gioitinh' value='$gioitinh[$i]' data-error='#errNm2' > $gioitinh[$i]";
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="information" for="phankhoa">Phân khoa <span style="color: red">*</span></label>
                    </td>
                    <td>
                        <select name="khoa" id="khoa" class="khoa" data-error="#errNm3">
                            <option value="">--Chọn phân khoa--</option>
                            <?php foreach ($khoa as $value) { ?>
                                <option value="<?php echo "$value"; ?>"><?php echo "$value" ?></option>

                            <?php } ?>
                            ?>
                        </select>

                    </td>
                </tr>
                <tr>
                    <td><label class="information" for="ngaysinh">Ngày sinh <span style="color: red">*</span></label></td>
                    <td>
                        <input class="ngaysinh" name="ngaysinh" type="text" placeholder="dd/mm/yyyy" data-error="#errNm4">
                    </td>
                </tr>
                <tr>
                    <td><label class="information label-diachi" for="diachi">Địa chỉ</label></td>
                    <td>
                        <textarea class="diachi" name="diachi"></textarea>
                    </td>
                </tr>
            </table>
            <div class="center-button">
                <button id="btnRegister" class="register-btn" type="submit">Đăng ký</button>
            </div>
        </form>
    </div>
    <script src="./script.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/additional-methods.js"></script>
</body>

</html>