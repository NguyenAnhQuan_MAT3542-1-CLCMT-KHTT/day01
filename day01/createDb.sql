CREATE DATABASE IF NOT EXISTS QLSV;
USE QLSV;
CREATE TABLE IF NOT EXISTS DMKHOA(
    MaKH varchar(6) NOT NULL,
    TenKhoa varchar(30) NOT NULL,
    PRIMARY KEY(MaKH)
);

CREATE TABLE IF NOT EXISTS SINHVIEN(
    MaSV varchar(6) NOT NULL,
    HoSV varchar(30) NOT NULL,
    TenSV varchar(15) NOT NULL,
    GioiTinh char(1) NOT NULL,
    NgaySinh DateTime NOT NULL,
    NoiSinh varchar(50) NOT NULL,
    DiaChi varchar(50) NOT NULL,
    MaKH varchar(6) NOT NULL,
    HocBong int NOT NULL,
    PRIMARY KEY(MaSV),
    FOREIGN KEY(MaKH) REFERENCES DMKHOA(MaKH)
);