document.getElementById("thanhpho").addEventListener("change", function () {
  var thanhpho = this.value;
  var quanSelect = document.getElementById("quan");

  // Xóa tất cả các tùy chọn hiện có trong selectbox quận.
  quanSelect.innerHTML = "<option value=''></option>";

  if (thanhpho === "HaNoi") {
    // Nếu chọn Hà Nội, thêm các tùy chọn quận tương ứng.
    var hanoiQuan = [
      "Hoàng Mai",
      "Thanh Trì",
      "Nam Từ Liêm",
      "Hà Đông",
      "Cầu Giấy",
    ];
    for (var i = 0; i < hanoiQuan.length; i++) {
      var option = document.createElement("option");
      option.text = hanoiQuan[i];
      option.value = hanoiQuan[i];
      quanSelect.appendChild(option);
    }
  } else if (thanhpho === "TpHCM") {
    // Nếu chọn Tp.HCM, thêm các tùy chọn quận tương ứng.
    var hcmcQuan = ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"];
    for (var i = 0; i < hcmcQuan.length; i++) {
      var option = document.createElement("option");
      option.text = hcmcQuan[i];
      option.value = hcmcQuan[i];
      quanSelect.appendChild(option);
    }
  }

  // Kích hoạt hoặc vô hiệu hóa selectbox quận tùy theo lựa chọn thành phố.
  quanSelect.disabled = thanhpho === "";
});


$(document).ready(function () {
    $(function () {
      $("#btnRegister").on("click", function () {
        $("#form-register").validate({
          rules: {
            hovaten: {
              required: true,
            },
            gioitinh: {
              required: true,
            },
            nam: {
              required: true,
            },
            thang: {
              required: true,
            },
            ngay: {
                required: true,
            },
            thanhpho: {
                required: true,
            },
            quan: {
                required: true,
            }
          },
          messages: {
            hovaten: {
              required: "Hãy nhập họ tên."
            },
            gioitinh: {
              required: "Hãy chọn giới tính."
            },
            nam: {
              required: "Hãy chọn ngày sinh."
            },
            thang: {
              required: "Hãy chọn ngày sinh.",
            },
            ngay: {
                required: "Hãy chọn ngày sinh",
            },
            thanhpho: {
                required: "Hãy chọn địa chỉ",
            },
            quan: {
                required: "Hãy chọn địa chỉ",
            }

          },
          errorPlacement: function (error, element) {
            var placement = $(element).data("error");
            console.log(placement);
            if (placement) {
              $(placement).append(error);
            } else {
              error.insertAfter(element);
            }
          },
        });
      });
    });
  });
  