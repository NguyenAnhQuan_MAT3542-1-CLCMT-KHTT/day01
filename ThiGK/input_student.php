<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trang Đăng Ký</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"></script>

</head>

<body>
    <div class="container form-register-container">
        <h3>Form đăng ký sinh viên</h3>
        <form action="./regist_student.php" method="POST" id="form-register" enctype="multipart/form-data">
            <div class="errorTxt">
                <p id="errNm1"></p>
                <p id="errNm2"></p>
                <p id="errNm3"></p>
                <p id="errNm4"></p>
            </div>

            <table>
                <tr>
                    <td>
                        <label class="information" for="hovaten">Họ và tên</span></label>
                    </td>
                    <td>
                        <div><input class="hovaten" name="hovaten" type="text" data-error="#errNm1"></div>
                    </td>
                </tr>
                <tr>
                    <td><label class="information" for="gioitinh">Giới tính</span></label></td>
                    <td>
                        <?php
                        echo "<input class='gioitinh' type='radio' name='gioitinh' value='nam' data-error='#errNm2' > Nam";
                        echo "<input class='gioitinh' type='radio' name='gioitinh' value='nu' data-error='#errNm2' > Nữ";
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="ngaysinh information" for="ngaysinh">Ngày sinh</label>
                    </td>
                    <td>
                        <label class="nam" for="nam"> Năm </label>
                        <select name="nam" id="nam" class="nam" data-error="#errNm3">
                            <option value=""></option>
                            <?php for ($i = 2023 - 40; $i <= 2023 - 15; $i++) { ?>
                                <option value="<?php echo "$i"; ?>"><?php echo "$i" ?></option>

                            <?php } ?>
                        </select>

                        <label for="thang">Tháng</label>
                        <select name="thang" id="thang" data-error="#errNm3">
                            <option value=""></option>
                            <?php
                            for ($i = 1; $i <= 12; $i++) { ?>
                                <option value="<?php echo "$i"; ?>"><?php echo "$i" ?></option>
                            <?php }
                            ?>
                        </select>

                        <label for="ngay">Ngày</label>
                        <select name="ngay" id="ngay" data-error="#errNm3">
                            <option value=""></option>
                            <?php
                            for ($i = 1; $i <= 31; $i++) { ?>
                                <option value="<?php echo "$i"; ?>"><?php echo "$i" ?></option>
                            <?php }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label class="diachi information" for="diachi">Địa chỉ</label></td>
                    <td>
                        <label for="thanhpho" style="
    margin-left: 20px;">Thành phố</label>
                        <select name=" thanhpho" id="thanhpho" data-error="#errNm4" style="
    width: 60px;">
                            <option value=""></option>
                            <option value="HaNoi">Hà Nội</option>
                            <option value="TpHCM">Tp.Hồ Chí Minh</option>
                        </select>
                        <label for="quan">Quận</label>
                        <select name="quan" id="quan" data-error="#errNm4" style="
    width: 60px;">
                            <option value=""></option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label class="information" for="thongtin">Thông tin khác</label></td>
                    <td>
                        <textarea style="
    margin-left: 20px;" name="thongtinkhac" id="thongtinkhac"></textarea>
                    </td>
                </tr>
            </table>
            <div class="center-button">
                <button id="btnRegister" name="btnRegister" class="register-btn" type="submit">Đăng ký</button>
            </div>
        </form>

        <script src="./script.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/additional-methods.js"></script>

</body>



</html>