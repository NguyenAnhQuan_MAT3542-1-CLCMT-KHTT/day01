<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"></script>

</head>

<body>
    <div class="container form-register-container">
        <form action="" method="POST" id="form-register" enctype="multipart/form-data">
            <table>
                <tr>
                    <td>
                        <label class="information" for="hovaten">Họ và tên</span></label>
                    </td>
                    <td>
                        <p><?php echo $_POST["hovaten"]; ?></p>
                    </td>
                </tr>
                <tr>
                    <td><label class="information" for="gioitinh">Giới tính</span></label></td>
                    <td>
                        <p><?php echo $_POST["gioitinh"]; ?></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="ngaysinh information" for="ngaysinh">Ngày sinh</label>
                    </td>
                    <td>
                        <p><?php echo $_POST["ngay"] . "/" . $_POST["thang"] . "/" . $_POST["nam"]; ?></p>
                    </td>
                </tr>
                <tr>
                    <td><label class="diachi information" for="diachi">Địa chỉ</label></td>
                    <td>
                        <p><?php echo $_POST["quan"] . " - " . $_POST["thanhpho"]; ?></p>
                    </td>
                </tr>
                <tr>
                    <td><label class="information" for="">Thông tin khác</label></td>
                    <td>
                        <p><?php echo $_POST["thongtinkhac"]; ?></p>
                    </td>
                </tr>
            </table>
            <div class="center-button">
                <button id="btnRegister" name="btnRegister" class="register-btn" type="submit">Đăng ký</button>
            </div>
        </form>
    </div>
</body>

</html>