<html>

<head>
    <title>List Students</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="./style2.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>

<body>
    <?php
    include("database.php");

    $khoa = [
        "MAT" => "Khoa học máy tính",
        "KDL" => "Khoa học dữ liệu"
    ];


    $query = $conn->prepare("SELECT ID, HoVaTen, PhanKhoa FROM students");
    $query->execute();
    $students = $query->get_result();
    $students = $students->fetch_all(MYSQLI_ASSOC);
    ?>
    <div class="container form-container">
        <form action="" method="post">

            <div class="tim-kiem-khoa">
                <label class="ten-khoa" for="ten-khoa">Khoa</label>
                <select name="khoa" id="khoa" class="khoa">
                    <option value=""></option>
                    <?php foreach ($khoa as $value) { ?>
                        <option value="<?php echo "$value"; ?>"><?php echo "$value" ?></option>

                    <?php } ?>
                    ?>
                </select>
            </div>
            <div class="tim-kiem-tu-khoa">
                <label>Từ khóa</label>
                <input type="text" class="tu-khoa">
            </div>
            <button class='btn-Search'>Tìm kiếm</button>
        </form>
    </div>

    <div class="container">
        <p class='count-student'> Số sinh viên tìm thấy: <?php echo count($students) ?></p>
        <button class="btn-Add" onclick="window.location.href='register.php';">Thêm</button>
    </div>
    <table>
        <tr>
            <td>No</td>
            <td>Tên sinh viên</td>
            <td>Khoa</td>
            <td>Action</td>
        </tr>
        <?php
        foreach ($students as $key => $value) {
        ?>
            <tr>
                <td>
                    <?php echo $value['ID'];
                    ?>
                </td>
                <td>
                    <?php echo $value['HoVaTen'];
                    ?>
                </td>
                <td>
                    <?php echo $value['PhanKhoa'];
                    ?>
                </td>
                <td>
                    <button class="btn-Del">Xóa</button>
                    <button class="btn-Mod">Sửa</button>
                </td>
            </tr>
        <?php } ?>
    </table>