<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trang đăng ký</title>
    <style>
        .form-register-container {
            width: 500px;
            height: 300px;
            border: 2px solid #527ea5;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            position: absolute;
            border-radius: 5px;
        }

        label {
            width: 110px;
            height: 35px;
            margin: 5px;
            padding: 10px;
            background-color: #5b9bd5;
            color: #fff;
            box-sizing: border-box;
            display: block;
            text-align: center;
            border: 2px solid #527ea5;
            font-family: 'Century';
        }

        /* input[type="radio"] {
            background-color: #5b9bd5;
        } */

        .gioitinh {
            margin-left: 20px;
        }

        .hovaten {
            width: 300px;
            height: 35px;
            border: 1px solid #000;
            margin: 10px;
            padding: 10px;
            box-sizing: border-box;
            display: flex;
            align-items: center;
            border: 2px solid #527ea5;
            font-family: 'Century';
        }

        .khoa {
            width: 145px;
            height: 35px;
            margin-left: 15px;
            border: 2px solid #527ea5;
        }

        .center-button {
            display: flex;
            justify-content: center;
            text-align: center;
        }

        .register-btn {
            background-color: #70ad47;
            width: 125px;
            height: 40px;
            margin-top: 20px;
            border: 2px solid #527ea5;
            border-radius: 8px;
            color: white;
            font-family: Century;
            font-size: 14px;
        }

        table {
            margin: 50px 20px 0 25px;
        }

        select {
            appearance: none;
            text-align-last:center;
        }
    </style>
</head>

<body>
    <?php
    $gioitinh = [
        0 => "Nam",
        1 => "Nữ"
    ];

    $khoa = [
        "MAT" => "Khoa học máy tính",
        "KDL" => "Khoa học dữ liệu"
    ];
    ?>
    <div class="container form-register-container">
        <form action="" method="POST">
            <table>
                <tr>
                    <td>
                        <label for="hovaten">Họ và tên</label>
                    </td>
                    <td>
                        <div><input class="hovaten" type="text"></div>
                    </td>
                </tr>
                <tr>
                    <td><label for="gioitinh">Giới tính</label></td>
                    <td>
                        <?php
                        for ($i = 0; $i < count($gioitinh); $i++) {
                            echo "<input class='gioitinh' type='radio' name='gioitinh' value='$gioitinh[$i]' > $gioitinh[$i]";
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="phankhoa">Phân khoa</label>
                    </td>
                    <td>
                        <select name="khoa" id="khoa" class="khoa">
                            <option value="--Chọn phân khoa--">--Chọn phân khoa--</option>
                            <?php foreach ($khoa as $value) { ?>
                                <option value="<?php echo "$value"; ?>"><?php echo "$value" ?></option>

                            <?php } ?>
                            ?>
                        </select>

                    </td>
                </tr>
            </table>
            <div class="center-button">
                <button class="register-btn" type="submit">Đăng ký</button>
            </div>
        </form>
    </div>
</body>

</html>