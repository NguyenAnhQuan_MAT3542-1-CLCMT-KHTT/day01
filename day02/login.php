<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trang Đăng Nhập</title>
    <style>
        body {
            font-family: 'Century';
        }

        .form-register-container {
            width: 600px;
            height: 300px;
            border: 2px solid #527ea5;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            position: absolute;
            border-radius: 5px;
        }

        .time p {
            background-color: #f2f2f2;
            height: 20px;
            width: 380px;
            padding: 10px;
            margin: 30px 50px 5px 80px;
            font-family: 'Century';
            font-weight: 300;
        }

        table {
            margin-left: 70px;
        }

        table input {
            width: 200px;
            height: 40px;
            border: 1px solid #000;
            margin: 10px;
            padding: 10px;
            box-sizing: border-box;
            display: flex;
            align-items: center;
            border: 2px solid #527ea5;
            font-family: 'Century';

        }

        .input-readonly {
            width: 175px;
            height: 40px;
            background-color: #5b9bd5;
            color: #fff;
        }

        .login {
            border-radius: 8px;
            border: 2px solid #527ea5;
            justify-content: center;
            width: 150px;
            height: 45px;
            margin-top: 40px;
            font-family: 'Century';
        }

        .center-button {
            display: flex;
            justify-content: center;
            text-align: center;
        }
    </style>
</head>

<body>
    <?php
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    setlocale(LC_TIME, 'vi_VN');
    $daysOfWeek = [
        1 => 'thứ 2',
        2 => 'thứ 3',
        3 => 'thứ 4',
        4 => 'thứ 5',
        5 => 'thứ 6',
        6 => 'thứ 7',
        7 => 'Chủ Nhật',
    ];
    ?>
    <div class="container form-register-container">
        <div class="time">
            <p><?php echo "Bây giờ là: " . date("H:i") . ", " . $daysOfWeek[date("N")] . " ngày " . date("d/m/Y"); ?></p>
        </div>
        <form action="process_login.php" method="POST">
            <table>
                <tr>
                    <td>
                        <input class="input-readonly" readonly value="Tên đăng nhập">
                    </td>
                    <td>
                        <div><input type="text"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input class="input-readonly" readonly value="Mật khẩu">
                    </td>
                    <td>
                        <input type="password">
                    </td>
                </tr>
            </table>
            <div class="center-button">
                <button class="input-readonly login" type="submit">Đăng nhập</button>
            </div>
        </form>
    </div>
</body>

</html>