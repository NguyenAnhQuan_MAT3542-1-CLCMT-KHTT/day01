<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trang đăng ký</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="./style.css">
</head>

<body>
    <?php
    session_start();
    if (isset($_POST['btnRegister'])) {
        $_SESSION['hovaten'] = $_POST["hovaten"];
        $_SESSION['gioitinh'] = $_POST["gioitinh"];
        $_SESSION['khoa'] = $_POST["khoa"];
        $_SESSION['ngaysinh'] = $_POST["ngaysinh"];
        $_SESSION['diachi'] = $_POST["diachi"];
        
        
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["image"]["name"]);
        move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
        $_SESSION['hinhanh'] = $target_file;

    }

    $ho_va_ten = isset($_SESSION['hovaten']) ? $_SESSION['hovaten'] : '';
    $gioi_tinh = isset($_SESSION['gioitinh']) ? $_SESSION['gioitinh'] : '';
    $khoa = isset($_SESSION['khoa']) ? $_SESSION['khoa'] : '';
    $ngay_sinh = isset($_SESSION['ngaysinh']) ? $_SESSION['ngaysinh'] : '';
    $dia_chi = isset($_SESSION['diachi']) ? $_SESSION['diachi'] : '';
    $hinh_anh = isset($_SESSION['hinhanh']) ? $_SESSION['hinhanh'] : '';
    
    ?>

    <div class="container form-register-container">
        <form action="" method="post" id="form-register">
            <table class="confirm-table">
                <tr>
                    <td>
                        <label class="information" for="hovaten">Họ và tên</label>
                    </td>
                    <td>
                        <p><?php echo $ho_va_ten; ?></p>
                    </td>
                </tr>
                <tr>
                    <td><label class="information" for="gioitinh">Giới tính</label></td>
                    <td>
                        <p><?php echo $gioi_tinh; ?></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="information" for="phankhoa">Phân khoa</label>
                    </td>
                    <td>
                        <p><?php echo $khoa; ?></p>
                    </td>
                </tr>
                <tr>
                    <td><label class="information" for="ngaysinh">Ngày sinh</label></td>
                    <td>
                        <p><?php echo $ngay_sinh; ?></p>
                    </td>
                </tr>
                <tr>
                    <td><label class="information label-diachi" for="diachi">Địa chỉ</label></td>
                    <td>
                        <p><?php echo $dia_chi; ?></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="information  label-image" for="img">Hình ảnh</label>
                    </td>
                    <td>
                        <img class='img' src="<?php echo $hinh_anh?>" alt='Uploaded Image' width = '100px' height='auto' >";
                    </td>
                </tr>
            </table>
            <div class="center-button">
                <button id="btnConfirm" name="btnConfirm" class="register-btn" type="submit">Xác nhận</button>
            </div>
        </form>
        <?php
        include 'database.php';


        if (isset($_POST["btnConfirm"])) {
            $sql = "INSERT INTO students (HoVaTen, GioiTinh, PhanKhoa, NgaySinh, DiaChi, HinhAnh) VALUES ('$ho_va_ten', '$gioi_tinh', '$khoa', STR_TO_DATE('$ngay_sinh','%d/%m/%Y'), '$dia_chi', '$hinh_anh')";

            if ($conn->query($sql) === TRUE) {
                echo "Dữ liệu đã được lưu vào cơ sở dữ liệu thành công!";
            } else {
                echo "Lỗi: " . $sql . "<br>" . $conn->error;
            }

            $conn->close();
        }
        ?>

    </div>
    <script src="./script.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/additional-methods.js"></script>
</body>

</html>